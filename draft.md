Le protocole IRC est un protocole basé sur des échanges de messages textes.

Le serveur doit :
- accepter plusieurs connextions.
- (pas clair) répercuter ces messages en mémoire sur une suite d'objets (Users - Channels - Messages)
- répondre aux clients correctement

Serveur != Network

On peut utiliser ircserv pour se connecter à un network existant. 
Weechat se connecte à ircserv et y trouve soit :
- un network en localhost
- un network distant auquel le serveur sera connecté

Le protocole est décrit par des RFC:
https://www.rfc-editor.org/rfc/rfc1459
https://www.rfc-editor.org/rfc/rfc2810
https://www.rfc-editor.org/rfc/rfc2813
https://www.rfc-editor.org/rfc/rfc2811
https://www.rfc-editor.org/rfc/rfc7194

Cet article aussi : 
https://modern.ircdocs.horse/
https://studiofreya.com/2015/06/27/a-simple-modern-irc-client-library-in-cpp-part-1-the-parser/

Grandes étapes:

- Récupération des hostname/ports
- Authentification d'un client au serveur
- Gestion des messages
- bonus: persist logs/channels ?

Objets:

- Network:
connect to an existing network
create network and set it up
- Channel:
props:
name,type,modes,topic,operators,users
- User:
props:
isOperator,channels
- MsgParser:
create a valid Message object if the string is valid 
- Message:

