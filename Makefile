NAME			= ircserv

FLAGS 			= -Wall -Wextra -Werror -std=c++98 -pedantic

CC				= g++

INC				= -I. -Isrc/

SRC_NAME		= 

OBJ_NAME		= $(SRC_NAME:.cpp=.o)

SRC_PATH		= src

OBJ_PATH		= .obj

SRC=$(addprefix $(SRC_PATH)/,$(SRC_NAME))

OBJS=$(addprefix $(OBJ_PATH)/,$(OBJ_NAME)) 

TEST=

all: makedir $(NAME)

makedir:
	@mkdir -p .obj

test: FLAGS += -g3 -fsanitize=address,undefined
test: TEST = test
test: makedir tests_$(NAME) $(NAME)

tests_$(NAME): $(OBJ_PATH)/test_main.o $(OBJS) irc.hpp
	$(CC) $(FLAGS) $(OBJ_PATH)/test_main.o  $(OBJS) $(INC) -o tests_$(NAME)

$(NAME): $(OBJ_PATH)/main.o $(OBJS) irc.hpp
	$(CC) $(FLAGS) $(OBJ_PATH)/main.o  $(OBJS) $(INC) -o $(NAME)

$(OBJ_PATH)/test_main.o: tests/main.cpp
	$(CC) $(FLAGS)  $(INC) -c tests/main.cpp -o $(OBJ_PATH)/test_main.o

$(OBJ_PATH)/main.o: main.cpp
	$(CC) $(FLAGS)  $(INC) -c main.cpp -o $(OBJ_PATH)/main.o

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.cpp
	$(CC) $(FLAGS)  $(INC) -c $< -o $@

clean:
	rm -fr $(OBJ_PATH)

fclean: clean
	rm -fr $(NAME)
	rm -fr tests_$(NAME)	
