#include "irc.hpp"
#include <string>
#include <iostream>
#include <cstring>
#include <stdlib.h>

int handle_options(int ac, char **av) {
    bool b_host = false;
    std::string net_host = "localhost";
    int port = 9042;
    std::string pass = "pass";
    int net_port = 9042;
    std::string net_pass = "pass";

    if ((ac > 4) || (ac == 1)) {
        std::cout << "Wrong number of arguments!" << std::endl;
        // usage
        return 1;
    }
    
    int i = 1;
    std::string str = av[i];
    std::string delim = ":";
    size_t pos_1 = 0;
    size_t pos_2 = 0;
    pos_1 = str.find(delim);
    if (pos_1 != std::string::npos) {
        net_host = str.substr(0, pos_1);
        b_host = true;
        pos_2 = str.find(delim, pos_1 + 1);
        if (pos_2 == std::string::npos) {
            std::cout << "Error: missing mandatory option, password!" << std::endl;
            return 1;
        }
        else {
            net_port = atoi(str.substr(pos_1 + 1, pos_2 - pos_1).c_str());
            net_pass = str.substr(pos_2 + 1, std::strlen(str.c_str()) - pos_2);
            i+= 1;
        }
    }
    if ((i == 1) && (b_host == false) && (ac >= 4)) {
        std::cout << "Wrong number of arguments!" << std::endl;  
        return 1;
    }

    if ((i < ac) && (i + 1 < ac)) {
        port = atoi(av[i]);
        pass = av[i + 1];
    }
    else {
        std::cout << "Wrong number of arguments!" << std::endl;
        return 1;
    }
    
    if (b_host == true) {
        /* connect to an existing network, setup auth for the client*/
        std::cout << "Connecting to existing network" << std::endl;
        std::cout << "host: " << net_host << std::endl;
        std::cout << "port: " << net_port << std::endl;
        std::cout << "password: " << net_pass << std::endl;
    }
    /* create a network in locahost, setup auth for the client */
    unused(port);
    unused(net_port);
    
    return 0;
}

int main(int ac, char **av) {
    if (handle_options(ac, av)) {
        return 1;
    }
    return 0;
}
